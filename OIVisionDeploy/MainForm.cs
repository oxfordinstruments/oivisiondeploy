﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Microsoft.NetMicroFramework.Tools;
using Microsoft.NetMicroFramework.Tools.MFDeployTool.Engine;
using Microsoft.NetMicroFramework.Tools.MFDeployTool.PlugIns;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Diagnostics;



namespace OIVisionDeploy
{
    public partial class MainForm : Form
    {
        const string firmwarever17 = "OIVision 1.5.0.622_1.7";  //must match the firmware name
        const string firmwarever16 = "OIVision 1.0.5.419_1.6";  //must match the firmware name
        const string firmwarever15 = "OIVision 1.5.0.622_1.5";  //must match the firmware name
        const string firmwarever14 = "OIVision 1.5.0.622_1.4";  //must match the firmware name
        string firmwarever = null;
        public byte[] mac;
        public System.Net.IPAddress ip;
        public System.Net.IPAddress gw;
        public System.Net.IPAddress subnet;
        public System.Net.IPAddress dns_p;
        public System.Net.IPAddress dns_s;
        public bool dhcp;
        string servicePath = null;
        string servicePath17 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), firmwarever17);
        string servicePath16 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), firmwarever16);
        string servicePath15 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), firmwarever15);
        string servicePath14 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), firmwarever14);
        const string res_name_start = "OIVisionDeploy.Resources.";
        public bool goodSettings = true;


        public MainForm()
        {
            InitializeComponent();
            //firmwareLbl.Text = firmwarever;
        }
        private void refreshBtn_Click(object sender, EventArgs e)
        {
            if (servicePath == null)
            {
                MessageBox.Show("Select a box revision first", "Revision Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (statusLbl.Text.Equals("Deploy Succeeded"))
            {
                MessageBox.Show("Restart OI Vision Deploy before flashing the next OI Vision Monitor", "Restart Required", MessageBoxButtons.OK);
                return;
            }
            deviceInfo();
        }
        private void flashBtn_Click(object sender, EventArgs e)
        {
            if (servicePath == null)
            {
                MessageBox.Show("Select a box revision first", "Revision Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (statusLbl.Text.Equals("Deploy Succeeded"))
            {
                MessageBox.Show("Restart OI Vision Deploy before flashing the next OI Vision Monitor", "Restart Required", MessageBoxButtons.OK);
                return;
            }
            statusBar.Value = 0;
            statusLbl.Text = "Flash Started";
            extractFirmware();
            statusLbl.Text = "Firmware Extracted";
            //Thread thread = new Thread(new ThreadStart(this.flashFirmware));
            //thread.Start();

            //thread.Join();
            if (!flashFirmware())
            {
                statusLbl.Text = "Deploy Failed";
                statusBar.Maximum = 100;
                statusBar.Value = 100;
                buttonPanel.Enabled = true;
                MessageBox.Show("Try unlpugging the usb cable and re-plugging it then click flash again", "Deploy Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            statusLbl.Text = "Deploy Succeeded";
            statusBar.Maximum = 100;
            statusBar.Value = 100;
            buttonPanel.Enabled = true;
        }
        private void MainForm_Shown(object sender, EventArgs e)
        {
            //deviceInfo();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            statusBar.Value = 0;
            MessageBox.Show("Deployment tool is only compatible with box version 1.4 or greater" + System.Environment.NewLine + "Installing this firmware on an incorrect box version can damage the box and possibly void any agreements or warranties.", "Firmware Check", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //Console.WriteLine(servicePath);
        }
        public bool deviceInfo()
        {
            if (servicePath == null)
            {
                MessageBox.Show("Select a box revision first", "Revision Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            try
            {
                statusLbl.Text = "Loading please wait!";
                statusBar.Value = 0;
                using (MFDeploy deploy = new MFDeploy())
                {
                    IList<MFPortDefinition> portDefs = deploy.EnumPorts(TransportType.USB);
                    statusBar.Value = 25;
                    if (portDefs.Count == 0)
                    {
                        Console.WriteLine("No Devices Found");
                        statusLbl.Text = "No Device Found";
                        statusBar.Value = 0;
                        flashBtn.Enabled = false;
                        deviceLbl.Text = "No Device Found";
                        macLbl.Text = "00-00-00-00-00-00";
                        return false;
                    }

                    if (portDefs[0].Name == "NetduinoPlus_NetduinoPlus")
                    {
                        deviceLbl.Text = "OIVision Magnet Monitor";
                    }

                    using (MFDevice device = deploy.Connect(portDefs[0]))
                    {
                        MFNetworkConfiguration m_cfg = new MFNetworkConfiguration(device);
                        MFConfigHelper m_cfgHelper = new MFConfigHelper(device);

                        m_cfg.Load();

                        m_cfgHelper.MaintainConnection = true;
                        statusBar.Value = 75;
                        macLbl.Text = MacToDashedHex(m_cfg.MacAddress);

                        mac = m_cfg.MacAddress;
                      
                        statusBar.Value = 100;
                        flashBtn.Enabled = true;
                        statusLbl.Text = "Ready to flash";
                        
                    }
                   
                }
                return true;
            }
            catch (MFDeviceNoResponseException e)
            {
                Console.WriteLine(e.ToString());
                statusLbl.Text = "No Device Response";
                statusBar.Value = 0;
                return false;
            }
            catch (MFDeviceInUseException e)
            {
                Console.WriteLine(e.ToString());
                statusLbl.Text = "Device Already Insuse";
                statusBar.Value = 0;
                return false;
            }
            
            
           
            
        }//end deviceInfo
        public bool extractFirmware()
        {
            if (servicePath == null)
            {
                MessageBox.Show("Select a box revision first", "Revision Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            Console.WriteLine(servicePath);
            Directory.CreateDirectory(servicePath);
            string resourceFile;      
            var assembly = Assembly.GetExecutingAssembly();
            foreach (var resourceName in assembly.GetManifestResourceNames())
            {
                Console.WriteLine("Found Resource: " + resourceName);
                if(resourceName.Contains(res_name_start))
                {
                    Console.WriteLine("Extracted Resource: "+resourceName);
                    resourceFile = Path.Combine(servicePath, resourceName.Replace(res_name_start, ""));
                    var memoryStream = new MemoryStream();
                    using(var stream = assembly.GetManifestResourceStream(resourceName))
                    {
                        stream.CopyTo(memoryStream);
                        File.WriteAllBytes(resourceFile, memoryStream.ToArray());
                    }
                }
            }
            return true;
        }        
        public bool flashFirmware()
        {
            if(servicePath == null)
            {
                MessageBox.Show("Select a box revision first", "Revision Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            try
            {
                buttonPanel.Enabled = false;
                using (MFDeploy deploy = new MFDeploy())
                {
                    IList<MFPortDefinition> portDefs = deploy.EnumPorts(TransportType.USB);

                    if (portDefs.Count == 0)
                    {
                        Console.WriteLine("No Device Found");
                        statusLbl.Text = "No Device found";
                        buttonPanel.Enabled = false;
                        return false;
                    }

                    using (MFDevice device = deploy.Connect(portDefs[0]))
                    {
                        device.Reboot(true);
                        device.OnProgress += new MFDevice.OnProgressHandler(OnStatus);
                        Collection<string> m_files = new Collection<string>();
                        List<uint> executionPoints = new List<uint>();
                        
                        int cnt = 0;
                        

                        //string resourceFile1 = Path.Combine(servicePath, "ER_CONFIG");
                        //string resourceFile2 = Path.Combine(servicePath, "ER_FLASH");
                        string resourceFile3 = Path.Combine(servicePath, firmwarever + ".hex");
                        string resourceFileSig = Path.Combine(servicePath, firmwarever + ".sig");
                        
                        string[] m_sigFiles = new string[3]{resourceFileSig,"",""};
                        //m_files.Add(resourceFile1);
                        //m_files.Add(resourceFile2);
                        m_files.Add(resourceFile3);
                        
                        
                        foreach (string file in m_files)
                        {
                            uint entry = 0;

                            if (!device.Deploy(file, m_sigFiles[cnt++], ref entry))
                            {
                                throw new MFDeployDeployFailureException();
                            }

                            if (entry != 0)
                            {
                                executionPoints.Add(entry);
                            }
                        }
                        executionPoints.Add(0);

                        OnStatus(100, 100, "Executing Application");
                        foreach (uint addr in executionPoints)
                        {
                            OnStatus(100, 100, "Executing Application " + addr.ToString());
                            if (device.Execute(addr)) break;
                        }


                        device.Reboot(false);
                    }
                }
                return true;
            }
            catch (MFDeviceNoResponseException e)
            {
                Console.WriteLine("No Response: " + e.ToString());
                statusLbl.Text = "No Device Response";
                statusBar.Value = 0;
                return false;
            }
            catch (MFDeviceInUseException e)
            {
                Console.WriteLine(e.ToString());
                statusLbl.Text = "Device Already Insuse";
                statusBar.Value = 0;
                return false;
            }
            catch (MFTinyBooterConnectionFailureException e)
            {
                Console.WriteLine(e.ToString());
                statusLbl.Text = "Device Connect Failure";
                statusBar.Value = 0;
                return false;
            }
            catch (MFInvalidFileFormatException e)
            {
                Console.WriteLine(e.ToString());
                statusLbl.Text = "Invalid Firmware";
                statusBar.Value = 0;
                return false;
            }
            catch (MFDeployDeployFailureException e)
            {
                Console.WriteLine(e.ToString());
                return false;

            }
        }//end flashfirmware
        private string MacToDashedHex(byte[] mac_addr)
        {
            string str = "";

            for (int i = 0; i < mac_addr.Length; i++)
            {
                str += string.Format("{0:x02}-", mac_addr[i]);
            }
            return str.TrimEnd('-');
        }
        private void OnStatus(long value, long total, string status)
        {
            int val, tot;
            //Console.WriteLine("Progress");
           
            if (total > 100)
            {
                val = (int)(value / 100);
                tot = (int)(total / 100);
            }
            else
            {
                val = (int)value;
                tot = (int)total;
            }
            statusLbl.Invoke((MethodInvoker)delegate
            {
                statusLbl.Text = status;
                statusLbl.Invalidate();
                statusBar.Maximum = tot;
                statusBar.Value = val;
                statusBar.Invalidate();
                this.Update();
            });
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (servicePath == null)
            {
                MessageBox.Show("Select a box revision first", "Revision Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                string[] files = Directory.GetFiles(servicePath);
                foreach (string file in files)
                {
                    Console.WriteLine("Removing: "+file);
                    File.Delete(file);
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.ToString());
            }
            Process.GetCurrentProcess().Kill();
        }
        private void cbFirmware_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;

            Console.WriteLine(cb.SelectedItem.ToString());
            string sel = cb.SelectedItem.ToString();
            switch (sel)
            {
                case "1.7":
                    firmwarever = firmwarever17;
                    servicePath = servicePath17;
                    break;
                case "1.6":
                    firmwarever = firmwarever16;
                    servicePath = servicePath16;
                    break;
                case "1.5":
                    firmwarever = firmwarever15;
                    servicePath = servicePath15;
                    break;
                case "1.4":
                    firmwarever = firmwarever14;
                    servicePath = servicePath14;
                    break;
                default:
                    break;
            }
            deviceInfo();

        }
    }
    [Serializable]
    public class MFDeployDeployFailureException : Exception
    {
        public override string Message { get { return "error"; } }
    }
}
